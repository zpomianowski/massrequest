# MassRequest

Nazwa trochę do czapy, ale no cóż. Jakoś to nazwać musiałem. Programik to nic innego jak
pająk, który odwiedza internet na masową skalę. Sporo można ustwić w pliku `massrequest/settings.py`
, mam tu na myśli głównie limity: requestów, warunki zamknięcia pająka.

### Instalacja

Na razie trzeba mieć _pythona_ i dla wygody _gita_. Najlepiej zrobić sobie wirtualne środowisko:
- tworzymy folder na cały projekt `mkdir massrequest && cd massrequest`
- ściągamy do tego folderu repo: `git clone https://saut@bitbucket.org/polsat/massrequest`
- obok tworzymy izolowane środowisko pythona: `virtualenv venv`
- aktywujemy je: `source venv/bin/activate`
- instalujemy depsy: `pip install -r requirements.txt`
- wchodzimy do repo gita: `cd massrequest`

### Użycie

Dwa kroki:
- aktywujemy venv: `source venv/bin/activate`
- odpalamy w venv pająka: `python runner.py`

Pająk wg limitów i ustawień w pliku `massrequest/settings.py` odpala pająka
 `massrequest/cpspider.py`. Zaczyna od strony _www.cyfrowypolsat.pl_ i potem
 jak po nitce do kłębuszka... praktycznie skanuje cały internet.

 Jak? Dosyć prosto! Każda większa strona ma odnośniki do innych stron, te zaś jeszcze do innych.
 Tym sposobem w nieskończoność można pozyskiwać nowe URLe.

### Najważniejsze ustawienia

~~~~
:::python
# Limit działania pająka w sekundach, po X sekundach przerywa pracę
CLOSESPIDER_TIMEOUT = 300
# Limit odwiedzonych stron
CLOSESPIDER_PAGECOUNT = 10
# Limit błędów podczas działania pająka, normalnie nie przejmuje się błędami tylko ciśnie dalej
CLOSESPIDER_ERRORCOUNT = 1
# Limit zeskrapowanych itemów; ten pająk scrapuje jedynie URLe, które będzie później odwiedzał
CLOSESPIDER_ITEMCOUNT = 1
# Limit jednoczesnych requestów
CONCURRENT_REQUESTS = 1000
# Limit jednoczesnych requestów dla odwiedzanej domeny
CONCURRENT_REQUESTS_PER_DOMAIN = 1000
# Limit jednoczesnych requestów dla danego IPka
CONCURRENT_REQUESTS_PER_IP = 1000
~~~~