from twisted.internet import reactor
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging
from scrapy.utils.project import get_project_settings
import os

try:
    os.remove('massrequest.log')
    os.remove('output.jl')
except:
    pass

configure_logging({'LOG_FORMAT': '%(levelname)s: %(message)s'})
runner = CrawlerRunner(get_project_settings())

d = runner.crawl('massrequest')
d.addBoth(lambda _: reactor.stop())
reactor.run()
