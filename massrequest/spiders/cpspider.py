import scrapy


class CpSpider(scrapy.Spider):
    name = "massrequest"

    start_urls = [
        'http://www.cyfrowypolsat.pl'
        ]

    def parse(self, response):
        for next_page in response.css('a').xpath('@href').extract():
            if 'http' not in next_page:
                continue
            yield {'url': next_page}
            yield scrapy.Request(
                response.urljoin(next_page), callback=self.parse)
